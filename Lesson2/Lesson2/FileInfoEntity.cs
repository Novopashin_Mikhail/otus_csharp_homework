using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace Lesson2
{
    /// <summary>
    /// Сущность для хранения информации о файле в БД
    /// </summary>
    public class FileInfoEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Наименование файла
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Добавлен ли файл после открытия задачи
        /// </summary>
        public bool IsAddedManual { get; set; }
        /// <summary>
        /// Был ли файл отправлен по электронной почте
        /// </summary>
        public bool IsSendedByEmail { get; set; }
        /// <summary>
        /// Бинарник файла в формате Base64
        /// </summary>
        public string ContentBase64 { get; set; }
        /// <summary>
        /// Конструктор без параметров
        /// </summary>
        public FileInfoEntity(){}
        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contentBase64"></param>
        public FileInfoEntity(string name, string contentBase64)
        {
            this.Name = Name;
            this.ContentBase64 = contentBase64;
        }
        /// <summary>
        /// Получение массива байт из base64
        /// </summary>
        public static byte[] ConvertContentBase64ToByteArray(string contentBase64) => Convert.FromBase64String(contentBase64);
        /// <summary>
        /// Явное преобразование сущности в data transfer object
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static implicit operator FileInfoDto(FileInfoEntity self)
        {
            return new FileInfoDto
            {
                Name = self.Name,
                IsAddedManual = self.IsAddedManual,
                IsSendedByEmail = self.IsAddedManual,
                ContentByteArray = ConvertContentBase64ToByteArray(self.ContentBase64)
            };
        }
        /// <summary>
        /// Переопределение ToString()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"Был создан фал с Id={Id}\n Name={Name}";
        }
    }
}