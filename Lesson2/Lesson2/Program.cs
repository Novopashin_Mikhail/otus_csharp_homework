﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileEntity = new FileInfoEntity
            {
                Id = 1,
                Name = "Test.txt",
                ContentBase64 = "VEVTVA==",
                IsAddedManual = null,
                IsSendedByEmail = null
            };

            Console.WriteLine(fileEntity.ToString());
            
            var fileDto = (FileInfoDto)fileEntity;
            var byteArray = fileDto.ContentByteArray;
            using (var stream = new MemoryStream(byteArray))
            {
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine("Содержимое файла:");
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }
    }
}