namespace Lesson2
{
    public class FileInfoDto
    {
        public string Name { get; set; }
        public bool? IsAddedManual { get; set; }
        public bool? IsSendedByEmail { get; set; }
        public byte[] ContentByteArray { get; set; }
    }
}